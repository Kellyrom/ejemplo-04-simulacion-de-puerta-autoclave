﻿
namespace Simulación_de_autoclave
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelpuertafrontal = new System.Windows.Forms.Panel();
            this.butabrir = new System.Windows.Forms.Button();
            this.butcerrar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Simulación_de_autoclave.Properties.Resources.AUTOOOCLAVE;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(204, 345);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.panel1.Controls.Add(this.butcerrar);
            this.panel1.Controls.Add(this.butabrir);
            this.panel1.Location = new System.Drawing.Point(215, 49);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(79, 125);
            this.panel1.TabIndex = 1;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // panelpuertafrontal
            // 
            this.panelpuertafrontal.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.panelpuertafrontal.Location = new System.Drawing.Point(35, 49);
            this.panelpuertafrontal.Name = "panelpuertafrontal";
            this.panelpuertafrontal.Size = new System.Drawing.Size(133, 125);
            this.panelpuertafrontal.TabIndex = 2;
            // 
            // butabrir
            // 
            this.butabrir.Location = new System.Drawing.Point(1, 12);
            this.butabrir.Name = "butabrir";
            this.butabrir.Size = new System.Drawing.Size(75, 23);
            this.butabrir.TabIndex = 0;
            this.butabrir.Text = "ABRIR ";
            this.butabrir.UseVisualStyleBackColor = true;
            this.butabrir.Click += new System.EventHandler(this.butabrir_Click);
            // 
            // butcerrar
            // 
            this.butcerrar.Location = new System.Drawing.Point(1, 90);
            this.butcerrar.Name = "butcerrar";
            this.butcerrar.Size = new System.Drawing.Size(75, 23);
            this.butcerrar.TabIndex = 1;
            this.butcerrar.Text = "CERRAR ";
            this.butcerrar.UseVisualStyleBackColor = true;
            this.butcerrar.Click += new System.EventHandler(this.butcerrar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(306, 345);
            this.Controls.Add(this.panelpuertafrontal);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Simulación de autoclave ";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button butcerrar;
        private System.Windows.Forms.Button butabrir;
        private System.Windows.Forms.Panel panelpuertafrontal;
    }
}

