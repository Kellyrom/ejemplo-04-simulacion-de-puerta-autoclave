﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simulación_de_autoclave
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void butabrir_Click(object sender, EventArgs e)
        {
            panelpuertafrontal.Height = panelpuertafrontal.Height - 5;
            panelpuertafrontal.Top = panelpuertafrontal.Top + 5; 
        }

        private void butcerrar_Click(object sender, EventArgs e)
        {
            panelpuertafrontal.Height = panelpuertafrontal.Height + 5;
            panelpuertafrontal.Top = panelpuertafrontal.Top - 5;
        }
    }
}
